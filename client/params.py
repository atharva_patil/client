#File paths
tar_dir  = '/home/siddharth/client/server_targets/'
sms_path = '/home/siddharth/client/sms_targets/'

#server params
url = 'http://localhost:5000/trigger_request/1/'

#int/float variables
sleeptime = 10
rts2_poll_time = 10

#RTS-2 Params
rts2_username = 'siddharth'
rts2_password = 'sid123'
xmlrpc_url = 'http://localhost:8889'
treshold_time = 10 # If current observation is taking more time than this then it won't be interrupted.
