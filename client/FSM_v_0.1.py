# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import time
import os
import subprocess

import master
import params as p
import jsoncalls as j
from request import http_req
from transitions import Machine
from transitions import State
from database import create_log
filename = '/var/log/testdaemon/testdaemon.log'
f = open(filename,'r')
global a
global b

class client(object):
    sleep = False
    queue   = 1 
    obs_id  = 0 
    # next_id = 0     
    # def check_dev(self):
    #     '''insert startup.py output here'''
    #     master.preliminary_connections()
    #     master.check_time()
    #     master.check_devices()
    #     master.check_weather()
    
    def check_lock(self):
        subprocess.call(["sudo","python","/usr/share/testdaemon/demonic_daemon.py","start"])
        a = ''
        b = subprocess.check_output(['tail', '-1', '/var/log/testdaemon/testdaemon.log'])
        if b != a:
            if 'lock found!' in b
            a = b
            print 'lock!'
            self.Lock_found
            time.sleep(p.locktime())
            machine.set_state('Routine_obs')
        else:
            print 'no lock'
            #self.Lock_not_found
            self.check_gw_trig


    def check_gw_trig(self):
        sleep = False
        while sleep == False:                   #System On loop
            current_id = j.getid()                    #gets current ongoin obs
            queue,target_file = http_req()              #Request for new target
            if queue == 1:
                print 'GW trig found!'
                source = j.create_obj(target_file)
                self.Time_check()
            else:
                target_file = select_file(p.sms_path)
                if target_file == None:
                    sleep = True
                else:
                    print 'GW sms trig found!'
                    source = j.create_obj(target_file)
                    self.Time_check()
        time.sleep(p.sleeptime)
        machine.set_state('Routine_obs')         
                    
    def Time_check(self):
        j.check(json,ID)
        if j.timer == 'now':
            print 'target getting executed now'
            self.GW_trig_now

        elif j.timer == 'now_with_backup':
            '''create backup'''
            print 'target getting executed now with backup'
            # create_log(target_file) #routine obs target backup
            self.GW_trig_now
        else:
            print 'waiting for current observation to finish'
            self.GW_trig_later

    def invoke_sleep(self):
        sleep = True
        j.sleep

    def actual_observation(self):
        obs_id = j.actual_obs()
        current_id = j.getid()
        while current_id == obs_id:
            time.sleep(p.rts2_poll_time)
            current_id = j.getid
        queue,target_file = http_req()
        if queue == 1:
            self.GW_trig_now
        else:
            machine.set_state('Routine_obs')
            time.sleep(p.locktime())

obs = client()
machine = Machine(obs)
state_A = State('Telescope_Off') 
state_B = State('DND')
state_C = State('Routine_obs')
state_D = State('First_GW_obs')
state_E = State('Last_GW_obs')
machine.add_states([state_A,state_B,state_C,state_D,state_E])
machine.add_transition('DND_end', source='DND', dest='Routine_obs') 
machine.add_transition('Lock_found', source='Routine_obs', dest='DND') ###check for lock
machine.add_transition('GW_trig_later', source='Routine_obs', dest='Routine_obs') ###if current not cancel
machine.add_transition('GW_trig_now', source='Routine_obs', dest='First_GW_obs') ####right now
machine.add_transition('Final_obs', source='First_GW_obs', dest='Last_GW_obs')
machine.add_transition('Dead', source='*', dest='Telescope_Off')
# machine.add_transition('Lock_not_found', source='Routine_obs', dest='Routine_obs') ###if lock not found
while True:    
    master.preliminary_connections()
    master.check_time()
    master.check_devices()
    master.check_weather()
    machine.set_state('Routine_obs')
    machine.on_enter_Routine_obs('check_lock')
    machine.on_enter_GW_trig_now('actual_observation')
    machine.on_enter_GW_trig_later('invoke_sleep')
