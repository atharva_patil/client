import sys
import logging
import time
from watchdog.events import PatternMatchingEventHandler
from watchdog.observers import Observer
import numpy as np
from astropy.io import fits
import requests



##from watchdog.events import FileSystemEventHandler  
#the above commenting out was done, but initially had used it based on a stackoverflow page http://stackoverflow.com/questions/18599339/python-watchdog-monitoring-file-for-changes
#from .models import Media


class MyHandler(PatternMatchingEventHandler):
    patterns=["*.fits"]

    def process(self, event):

        #the following commands obatin the basic meta-data from the image header using astropy-fits module     
        hdu_number = 0
        header = fits.getheader(event.src_path,hdu_number)
        TarID = str(header['TARGET'])
        ObjID = str(header['OBJECT'])
        RA_obs   = float(header['OBJRA'])
        DEC_obs  = float(header['OBJDEC'])
        Avg      = header['AVERAGE']
        SD       = header['STDEV']

        #data can now be written into a database/file. I have sent it directly to server through request.get library.

        payload = {'Target_ID':TarID, 'Object_ID':ObjID, 'RA':RA_obs, 'DEC':DEC_obs, 'Avg_Pixel_value':Avg,'Std_Dev_Pixel_Value':SD}
        
        print 'sending image data to server'

        r = requests.get('http://127.0.0.1:5000/client_report', params=payload)

        #One can see that the URL has been correctly encoded by printing the URL:
        print(r.url)


        data=[]
        data = [TarID, RA_obs, DEC_obs, Avg, SD]
        print 'For the targetID',TarID,'with observed RA', RA_obs,'and observed DEC', DEC_obs, 'the average and standard deviation of the image is', Avg, SD
      

        #f = open('imagedata.txt','a')
        #print 'writing data to file'         #f.write(data)
        #f.close

        return data
        
    ##this command looks for new files that are created in the directory, and then on them the above code is run. 
    def on_created(self, event):
        print 'got a new fits file'   
        self.process(event)


if __name__ == '__main__':
    args = sys.argv[1:]
    observer = Observer()
    observer.schedule(MyHandler(), path=args[0] if args else '.', recursive=True)  ##"recursive=True" ensures that watchdog runs on the folders within the working directories which is essential for our purpose as folders are going to be create by night within the main directory.
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()






